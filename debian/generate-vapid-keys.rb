vapid_key = Webpush.generate_key

File.open('/var/lib/pupilfirst/pupilfirst.env', 'a') do |file|
	file.puts "VAPID_PUBLIC_KEY=#{vapid_key.public_key}"
	file.puts "VAPID_PRIVATE_KEY=#{vapid_key.private_key}"
end
