#!/bin/sh

# Exit on error
set -e

check_db_status() {
	echo "Checking if the database is empty..."
	db_relations="$(LANG=C runuser -u postgres -- sh -c "psql pupilfirst_production -c \"\d\"" 2>&1)"
	test "$db_relations" = "No relations found." || \
	test "$db_relations" = "Did not find any relations."
}

# Check if the db is already present
if check_db_status ; then
echo "Initializing database..."
# enable the citext extension
    runuser -u postgres -- sh -c "psql -d pupilfirst_production -c \"CREATE EXTENSION IF NOT EXISTS citext;\""
# enable the pg_stat_statements extension
    runuser -u postgres -- sh -c "psql -d pupilfirst_production -c \"CREATE EXTENSION IF NOT EXISTS pg_stat_statements;\""
# enable the pg_trgm extension
    runuser -u postgres -- sh -c "psql -d pupilfirst_production -c \"CREATE EXTENSION IF NOT EXISTS pg_trgm;\""
	su pupilfirst -s /bin/sh -c 'bundle exec rake db:schema:load'
	su pupilfirst -s /bin/sh -c 'bundle exec rake db:seed'
	su pupilfirst -s /bin/sh -c 'bundle exec rake db:migrate'
else
	echo "pupilfirst_production database is not empty, skipping database setup"
	echo "Running migrations..."
        su pupilfirst -s /bin/sh -c 'bundle exec rake db:migrate'
fi
