#!/bin/sh

# Exit on error
set -e

# Note: This script should be invoked as pupilfirst user
# Copy source tree to a directory with write access and build
echo "Copying source tree to /var..."
cd /var/lib/pupilfirst
rm -rf build
mkdir build
rsync -a --exclude=public/assets --exclude=public/packs --exclude=app/javascript/locales.json /usr/share/pupilfirst/ build/

cd build
# Precompile assets
echo "Precompiling assets..."
# Need this for webpacker to find yarn command
export PATH=/var/lib/pupilfirst/build/bin:$PATH
bundle exec rake assets:precompile
